# einfalda
Command line utility to find duplicated files.
```
usage: java Einfalda path

  Example: java Einfalda /path/to/director
```

```
Duplicates:
  /Users/petur/duplicates/jack
  /Users/petur/duplicates/jane

Duplicates:
  /Users/petur/duplicates/alice
  /Users/petur/duplicates/bob
  /Users/petur/duplicates/bob2

Space eaten up by duplicates: 13 B
```
