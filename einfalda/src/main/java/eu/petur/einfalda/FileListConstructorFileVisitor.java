package eu.petur.einfalda;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

/**
 * Constructs a list of files available from a directory.
 */
public class FileListConstructorFileVisitor extends SimpleFileVisitor<Path> {

  private List<Path> pathsFound = new LinkedList<Path>();

  public List<Path> getPathsFound() {
    return pathsFound;
  }

  @Override
  public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
    return FileVisitResult.CONTINUE;
  }

  @Override
  public FileVisitResult visitFile(Path path, BasicFileAttributes fileAttributes) throws IOException {
    pathsFound.add(path);
    return FileVisitResult.CONTINUE;
  }

  @Override
  public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
    return FileVisitResult.CONTINUE;
  }

}

