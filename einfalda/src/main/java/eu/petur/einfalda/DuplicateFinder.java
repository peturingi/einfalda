package eu.petur.einfalda;

import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import org.apache.commons.lang3.Validate;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Finds content-wise file duplicates from a set of paths.
 */
public class DuplicateFinder {

  /*
   * A cache consists of two levels.
   * The first level has 0xFF slots.
   * The second level has 0xFF slots.
   * Each second level slot contains multiple files which
   * have been evaluated to be possible content-wise duplicates.
   */
  private ArrayList<ArrayList<LinkedList<FileInfo>>> cache;

  /**
   * Constructs a new DuplicateFinder.
   */
  public DuplicateFinder() {
    prepareCache();
  }

  /**
   * Prepares the cache for use.
   * The cache will be wiped if it has previously been populated.
   */
  private void prepareCache() {
    cache = new ArrayList<ArrayList<LinkedList<FileInfo>>>();
    /* FileInfo's are placed in the cache based on the content of certain bytes. Domain(byte) = [0x00;0xFF] */
    final int CACHE_SIZE = 0xFF + 1;
    for (int j = 0; j < CACHE_SIZE; j++) {
      final ArrayList<LinkedList<FileInfo>> lists = new ArrayList<LinkedList<FileInfo>>();
      for (int i = 0; i < CACHE_SIZE; i++)
        lists.add(new LinkedList<FileInfo>());
      cache.add(lists);
    }
  }

  /**
   * Add a file or a directory to the list of files to be searched for duplicates.
   * If a directory is added, all readable files found in its subtree will be evaluated.
   * @param path A path to a readable file or an executable directory.
   */
  public void addToSearch(final Path path) {
    Validate.isTrue(Utils.isReadableFileOrExecutableDirectory(path), "Path must be to an executable directory or a readable file.");
    if (path.toFile().isDirectory())
      addToSearch(getListOfFilePaths(path));
    else
      if (path.toFile().isFile())
        try {
          addToSearch(new FileInfo(path));
        } catch (IOException e) {
          System.err.println("Can not search " + path + " " + e.getMessage());
        }
  }

  private List<Path> getListOfFilePaths(final Path directory) {
    Objects.requireNonNull(directory);

    final FileListConstructorFileVisitor visitor = new FileListConstructorFileVisitor();
    try {
      Files.walkFileTree(directory, visitor);
    }
    catch (IOException e) {
      // This can not happen as visitor never throw up.
      e.printStackTrace();
      System.exit(ExitCode.EX_UNAVAILABLE.toInt());
    }
    return visitor.getPathsFound();
  }

  /**
   * Add a file or a directory to the list of files to be searched for duplicates.
   * If a directory is added, all readable files found in its subtree will be evaluated.
   * @param paths A list of path's to readable files or executable directories.
   */
  public void addToSearch(final List<Path> paths) {
    Validate.notNull(paths);
    List<FileInfo> fileInfos = FileInfo.getFileInfos(paths);
    for (FileInfo f: fileInfos)
      addToSearch(f);
  }

  private void addToSearch(final FileInfo fileInfo) {
    Objects.requireNonNull(fileInfo);
    /* Map the domain of a byte from [-128;127] to [255;0](reverse order). */
    int first = 0xFF & ~Byte.valueOf(fileInfo.getFirstByteFromMiddle());
    int second = 0xFF & ~Byte.valueOf(fileInfo.getSecondByteFromMiddle());

    if (!cache.get(first).get(second).contains(fileInfo))
      cache.get(first).get(second).add(fileInfo);
  }

  /**
   * Finds and returns all duplicates found within previously given paths.
   * @return A map containing duplicated files.
   * Each key points to a collection of files which are content-wise duplicates of each other.
   */
  public Multimap<Integer, Path> getDuplicates() {
    Validate.notNull(cache, "The cache has not been prepared.");
    LinkedHashMultimap<Integer, Path> duplicates = LinkedHashMultimap.create();
    Integer key = 0;
    for (ArrayList<LinkedList<FileInfo>> level2 : cache) {
      for (LinkedList<FileInfo> list : level2) {
        Set<FileInfo> dup = new HashSet<FileInfo>();
        again:
        for (FileInfo info : list) {
          dup.add(info);// Assume file has duplicates.
          for (FileInfo possibleDuplicate : list) {
            // Do not evaluate against self.
            if (info.getPath().equals(possibleDuplicate.getPath()))
              continue;
            if (info.equals(possibleDuplicate))
              // Ignore empty files.
              if (possibleDuplicate.getPath().toFile().length() > 0)
                dup.add(possibleDuplicate);
          }
          if (dup.size() > 1) {
            for (FileInfo fi : dup)
            duplicates.put(key, fi.getPath());
            key++;
            dup.clear();
            break again;
          }
          else
            dup.clear();
        }
      }
    }
    return duplicates;
  }
}
