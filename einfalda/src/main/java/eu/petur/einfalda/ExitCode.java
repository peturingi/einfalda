package eu.petur.einfalda;

/**
 * Exit Codes.
 */
public enum ExitCode {
  EX_USAGE(64),
  EX_UNAVAILABLE(69);

  private final int code;

  ExitCode(final int code) {
    this.code = code;
  }

  public int toInt() {
    return code;
  }
}
