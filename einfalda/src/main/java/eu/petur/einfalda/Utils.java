package eu.petur.einfalda;

import java.io.File;
import java.nio.file.Path;

public class Utils {
  static public boolean isReadableFileOrExecutableDirectory(final Path path) {
    if (path == null) return false;
    final File file = path.toFile();
    final boolean executableDir = file.isDirectory() && file.canExecute();
    final boolean readableFile = file.isFile() && file.canRead();
    return executableDir || readableFile;
  }
}
