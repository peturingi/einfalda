package eu.petur.einfalda;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.Validate;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * A FileInfo represents a readable regular file on the filesystem.
 * It can be used to perform a performance-enhanced byte-wise comparison of two files.
 */
public class FileInfo {

  /**
   * The size of the buffer used to cache the middle of a file.
   */
  static private final int BUFFER_SIZE = 1024;

  /**
   * The size of the buffers used during comparisons.
   * Two of those are used when comparing files.
   */
  static private final int BUFFER_SIZE_COMPARISON = 1024*1024;

  /**
   * Path to a regular, readable file.
   */
  private final Path path;

  /**
   * Lenth of the file pointed to by this path.
   */
  private final long length;

  /**
   * Holds the the content from the middle of a file.
   */
  private final byte[] buffer = new byte[BUFFER_SIZE];

  public Path getPath() {
    return path;
  }

  /**
   * Constructs a FileInfo based on a file at path.
   *
   * @param path A path to a readable file or an executable directory.
   * @throws IOException An I/O exception occured while trying to populate the buffer.
   */
  public FileInfo(final Path path) throws IOException {
    Validate.isTrue(Utils.isReadableFileOrExecutableDirectory(path), "Path must be to an executable directory or a readable file.");
    final File file = path.toFile();
    this.path = path;
    length = file.length();

    FileInputStream is = null;
    try {
      is = new FileInputStream(file);
      /* Read from the middle of the file as many files have a common beginning.
      The content from the midway of two files is more likely to be different. */
      is.skip(length / 2);
      is.read(buffer);
    } catch (IOException readException) {
      throw readException;
    } finally {
      IOUtils.closeQuietly(is);
    }
  }

  /**
   * Get a byte from the middle of the file.
   * @return The byte at the middle of the file, or 0 if the file is empty.
   */
  public byte getFirstByteFromMiddle() {
    return length > 0 ? buffer[0] : 0;
  }


  /**
   * Get the first byte from the middle of the file
   * @return The first byte from the middle of the file, or 0 if the file has length 1.
   */
  public byte getSecondByteFromMiddle() {
    return length > 1 ? buffer[1] : 0;
  }

  /**
   * Compares the contents of a file represented by this FileInfo
   * to the contents of another file represented by a FileInfo.
   * @param fileInfo The fileInfo representing the file to be compared against.
   * @return true if the files have the same content; false otherwise.
   */
  public boolean equals(final FileInfo fileInfo) {
    Validate.notNull(fileInfo);
    if (this.length == fileInfo.length)
      if (Arrays.equals(this.buffer, fileInfo.buffer))
        if (hasSameContent(this.path.toFile(), fileInfo.path.toFile()))
          return true;
    return false;
  }

  /**
   * Compares the contents of a file to the contents of another file.
   * @param file The fileInfo representing the file to be compared against.
   * @param file2 the other fileInfo..
   * @return true if the files have the same content; false if files are different,
   * if one of them could not be found or if they could not be compared due to IO errors, or otherwise.
   */
  private boolean hasSameContent(final File file, final File file2) {
    Validate.notNull(file);
    Validate.notNull(file2);

    FileInputStream fileIs = null;
    FileInputStream file2Is = null;
    boolean sameContent = false;

    try {
      fileIs = new FileInputStream(file);
      file2Is = new FileInputStream(file2);

      final byte[] bytes = new byte[BUFFER_SIZE_COMPARISON];
      final byte[] bytes2 = new byte[BUFFER_SIZE_COMPARISON];

      do {
        final int read = fileIs.read(bytes);
        final int read2 = file2Is.read(bytes2);

        if (!Arrays.equals(bytes, bytes2)) break;
        else if (read != read2) /* one, but not other, is at EOF */ break;
        else if (read == -1) {
          /* both at EOF */
          sameContent = true;
          break;
        }
      } while (true);

    } catch (FileNotFoundException e) {
      System.err.println("File not found: " + e.getMessage());
    } catch (IOException e) {
      /* No can do, assume files are not duplicates. */
      System.err.println("Could not compare " + file.getAbsolutePath() + " against " + file2.getAbsolutePath());
    } finally {
      IOUtils.closeQuietly(fileIs);
      IOUtils.closeQuietly(file2Is);
    }
    return sameContent;
  }

  /**
   * Constructs a FileInfo representation of Paths.
   * A fileInfo will not be constructed for a path which does not represent a file
   * or which represents a file which the user can not read.
   * @param paths List of paths.
   * @return List of FileInfo constructed based on the input paths.
   */
  static public List<FileInfo> getFileInfos(final List<Path> paths) {
    Objects.requireNonNull(paths);

    List<FileInfo> fileInfos = new LinkedList<FileInfo>();
    for (Path p : paths) {
      if (p.toFile().isFile() && p.toFile().canRead()) {
        try {
          fileInfos.add(new FileInfo(p));
        } catch (IOException readError) {
          /* Should not happen unless
           * file permission was changed just after we checked canRead().
           * or file was deleted and something else put in its place. */
          System.err.println(readError.getMessage());
         }
      }
    }
    return fileInfos;
  }
}
