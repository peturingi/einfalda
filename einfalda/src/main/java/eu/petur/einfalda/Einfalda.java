package eu.petur.einfalda;

import com.google.common.collect.Multimap;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.*;

public class Einfalda {

  @Argument
  private List<String> arguments = new ArrayList<String>();

  public static void main(String[] args) throws IOException {
    new Einfalda().doMain(args);
  }

  public void doMain(String[] args) throws IOException {
    CmdLineParser parser = new CmdLineParser(this);
    try {
      parser.parseArgument(args);
      if (arguments.isEmpty() || arguments.size() > 1)
        throw new CmdLineException(parser, "");
    } catch (CmdLineException e) {
      System.err.println(e.getMessage());
      System.err.println("usage: java Einfalda path");
      parser.printUsage(System.err);
      System.err.println();
      System.err.println("  Example: java Einfalda /path/to/directory");
      return;
    }

    // TODO Allow more than a single argument to be used.
    final Path directory = Paths.get(arguments.get(0));
    if (!directory.toFile().isDirectory()) {
      System.err.println("Error - directory not found: " + directory);
      System.exit(ExitCode.EX_USAGE.toInt());
    }

    final DuplicateFinder duplicateFinder = new DuplicateFinder();
    duplicateFinder.addToSearch(directory);
    final Multimap<Integer, Path> duplicates = duplicateFinder.getDuplicates();
    print(duplicates);
  }

  private void print(final Multimap<Integer, Path> duplicates) {
    long spaceWasted = 0;
    for (Integer key: duplicates.keySet()) {
      final Collection<Path> paths = duplicates.get(key);
      spaceWasted += paths.iterator().next().toFile().length() * (paths.size()-1);
      System.out.println("Duplicates:");
      for (Path path : paths)
        System.out.println("  " + path);
      System.out.println();
    }
    System.out.println("Space eaten up by duplicates: " + readableFileSize(spaceWasted));
  }

  /**
   * @author Mr. Ed
   * @see <a href="http://stackoverflow.com/questions/3263892/format-file-size-as-mb-gb-etc">stackoverflow</a>
   */
  private String readableFileSize(long size) {
    if(size <= 0) return "0";
    final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
    int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
    return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
  }
}
